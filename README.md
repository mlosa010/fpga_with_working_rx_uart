to make this thing work you need 

1) Elbert V2 FPGA development board from Namato labs
2) Xilinx ISE (https://drive.google.com/file/d/16LvWnUCsDIu1Dk0sfreE5qd_z3V6h-Uu/view?usp=sharing)<-instructions to get ISE

once you have these things then you can open the project in the xilinx ISE and generate a bin, once you generate a .bin then you can upload it to the FPGA using the Elbert V2 configuration
tool found on the product page for the Elbert V2 